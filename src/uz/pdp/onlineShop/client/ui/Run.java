package uz.pdp.onlineShop.client.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import uz.pdp.onlineShop.server.database.Database;
import uz.pdp.onlineShop.server.dtos.*;
import uz.pdp.onlineShop.server.enums.UserStatus;
import uz.pdp.onlineShop.server.enums.Weight;
import uz.pdp.onlineShop.server.model.Product;
import uz.pdp.onlineShop.server.model.User;
import uz.pdp.onlineShop.server.service.UserService;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Run {
    static File productsFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\products.json");
    static File usersFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\users.json");
    static File changedHistorysFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\changedHistory.json");
    static File targetHistorysFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\targetHistory.json");


    static Scanner scanner = new Scanner(System.in);
    static ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) {


        String operation;

        do {
            System.out.println("""
                    \n \t\t\t WELCOME
                     1. Sign in
                     2. Registor
                     3. Show products""");
            System.out.print(" ?: ");
            operation = scanner.nextLine();
            switch (operation) {
                case "1" -> {
                    signIn(scanner);
                }
                case "2" -> {
                    register(scanner);
                }
                case "3" -> {
                    showProducts();
                }
            }

        } while (true);
    }

    private static void signIn(Scanner scanner) {
        String operation;
        String username;
        String password;
        UserService userService = new UserService().getInstance();
        System.out.print("\nEnter user: ");
        username = scanner.nextLine();
        System.out.print("Enter password: ");
        password = scanner.nextLine();
        CheckUser userDto = new CheckUser(username, password);
        ResponsDTO responsDTO = userService.checkUser(userDto);
        if (!responsDTO.succes()) {
            System.out.println("Username or password is wrong!!!");
            return;
        }

        User user = userService.findUser(username);
        System.out.println("\nWelcome " + username + " !!!  Your status " + user.getUserStatus().toString().toUpperCase());
        if (user.getUserStatus().equals(UserStatus.USER)) {
            do {
                System.out.println("""

                        1. Show products
                        2. Buy product
                        3. Show balance
                        4. Fill balance
                        0. Come back""");
                System.out.print(" ?: ");
                operation = scanner.nextLine();
                if (operation.equals("0")) return;
                switch (operation) {
                    case "1" -> {
                        showProduct(userService);
                    }
                    case "2" -> {
                        buyProduct(scanner, userService, user);
                    }
                    case "3" -> {
                        System.out.println("\n Your balance: " + user.getBalance());
                    }
                    case "4" -> {
                        fillBalance(scanner, userService, user);

                    }
                    default -> {
                        System.out.println("An error occurred, please check and re-enter!\n ");
                    }
                }

            } while (true);
        } else if (user.getUserStatus().equals(UserStatus.ADMIN)) {
            label2:
            do {

                System.out.println("""

                        1. Add product
                        2. Show products list
                        3. Making changes to the product
                        4. Show trade history
                        5. Show Users list
                        0. Come back""");
                System.out.print(" ?: ");
                operation = scanner.nextLine();
                if (operation.equals("0")) return;

                switch (operation) {
                    case "1" -> {
                        addProduct(scanner, userService, user);

                    }
                    case "2" -> {
                        showProducts();
                    }
                    case "3" -> {
                        makingChangesToTheProduct(scanner, userService, user);

                    }
                    case "4" -> {
                        showTradeHistory(userService, user);

                    }
                    case "5" -> {
                        showUsersList(userService);
                    }
                    default -> {
                        System.out.println("An error occurred, please check and re-enter!\n ");
                    }
                }

            } while (true);

        }

        if (user.getUserStatus().equals(UserStatus.SUPERADMIN)) {
            do {

                System.out.println("""

                        1. Show Admins list
                        2. Show Users list
                        3. Show products list
                        4. Show trade history
                        5. Show changes of product parametry
                        6. Make admin
                        7. Make user from admin
                        0. Come back""");
                System.out.print(" ?: ");
                operation = scanner.nextLine();
                if (operation.equals("0")) return;
                switch (operation) {
                    case "1" -> {
                        showAdminsList(userService);
                    }
                    case "2" -> {
                        showUsersList(userService);
//                    List<User> userList=userService.listOfUsers();
//                    if (userList.size()==0) {
//                        System.out.println("Users list is empty");
//                        return;
//                    }
//                    for (User user1 : userList) {
//                        System.out.println(user1);
//                    }
                    }
                    case "3" -> {
                        showProduct(userService);
//                    List<Product> productList=Database.productList;
//                    if (productList.size()==0) {
//                        System.out.println("Product list is empty");
//                        return;
//                    }
//                    for (Product product : productList) {
//                        System.out.println(product);
//                    }
                    }
                    case "4" -> {
                        showTradeHistory(userService, user);
//                    List<TradeHistory> tradeHistories=Database.getTradeHistories();
//                    if (tradeHistories.size()==0) {
//                        System.out.println("Trade histories list is empty");
//                        return;
//                    }
//                    for (TradeHistory tradeHistory : tradeHistories) {
//                        System.out.println(tradeHistories);
//                    }
                    }
                    case "5" -> {
                        showChangesParametryOfProducts();
                    }
                    case "6" -> {
                        System.out.print("Enter user name ");
                        String lastUserName = scanner.nextLine();
                        ResponsDTO responsDTO1 = UserService.makeAdmin(lastUserName);
                    }
                    case "7" -> {
                        System.out.print("Enter user name ");
                        String lastAdminName = scanner.nextLine();
                        ResponsDTO responsDTO1 = UserService.makeUser(lastAdminName);
                    }

                }
            } while (true);
        }
    }

    private static void showChangesParametryOfProducts() {
        List<HistoryOfChangesDTO> historyOfChanges = Database.getHistoryOfChanges();
        if (historyOfChanges.size() == -0) {
            System.out.println("History of changes list is empty");
            return;
        }
        for (HistoryOfChangesDTO historyOfChange : historyOfChanges) {
            System.out.println(historyOfChange);
        }
    }

    private static void showAdminsList(UserService userService) {
        List<User> adminsList = userService.listOfAdmins();
        if (adminsList.size() == 0) {
            System.out.println("Admins list is empty");
            return;
        }
        for (User user1 : adminsList) {
            System.out.println(user1);
        }
    }

    private static void showUsersList(UserService userService) {
        List<User> users = userService.listOfUsers();
        for (User user1 : users) {
            System.out.println(user1);
        }
    }

    private static void showTradeHistory(UserService userService, User user) {
        List<TradeHistory> tradeHistories = userService.getTradeHistory(user);
        if (tradeHistories == null || tradeHistories.size() == 0) {
            System.out.println("History is empty! ");
            return;
        }
        for (TradeHistory history : tradeHistories) {
            System.out.println(history);
        }
    }

    private static void makingChangesToTheProduct(Scanner scanner, UserService userService, User user) {
        String operation;
        do {
            try {
                List<Product>  availableproducts = objectMapper.readValue(productsFile,new TypeReference<List<Product>>() {
                });
            if (availableproducts != null) {
                for (Product availableproduct : availableproducts) {
                    System.out.println(availableproduct);
                }
                System.out.print("Choose product:\n Enter the name of product: ");
                String nameOfProduct = scanner.nextLine();
                System.out.print(" Enter the color of product: ");
                String colorOfProduct = scanner.nextLine();
                Product product = userService.findProduct(nameOfProduct, colorOfProduct);
                if (product != null) {
                    System.out.println("""

                            Which one do you want to change?
                            1. Add amount
                            2. Change price
                            3. Change Weight
                            0. Come back""");
                    System.out.print(" ?: ");

                    operation = scanner.nextLine();
                    if (operation.equals("0")) return;
                    else if (operation.equals("1")) {
                        System.out.print("How much do you want to add? ");
                        double addAmount = scanner.nextDouble();
                        scanner.nextLine();
                        userService.addAmountToProduct(product, addAmount, user);
                        return;
                    } else if (operation.equals("2")) {
                        System.out.print("How much do you want to set the price? ");
                        double newPrice = scanner.nextDouble();
                        userService.changePriceOfProduct(product, newPrice, user);
                        return;
                    } else if (operation.equals("3")) {
                        System.out.print("What type do you want to convert the measurement to?" +
                                " 1=KG; 2= PIECE;  (By default is KG)");
                        String choose = scanner.nextLine();
                        Weight weight = Weight.KG;
                        if (choose.equals("2")) weight = Weight.PIECE;
                        userService.changeWeightOfProduct(product, weight, user);
                        return;
                    } else System.out.println("You made the wrong choice! ");
                }
            }
            } catch (IOException e) {
                e.printStackTrace();
            }
//            {
//                System.out.println("Product list is empty");
//            }


        } while (true);
    }

    private static void showProducts() {
//        List<Product> productList = Database.productList;
//        for (Product product : productList) {
//            System.out.println(product);
//        }
        List<Product> productList1 = null;
        try {
            productList1 = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (productList1 != null) {
            for (Product product : productList1) {
                System.out.println(product);
            }
        }

    }


    private static void addProduct(Scanner scanner, UserService userService, User user) {
        String nameOfProduct;
        String colorOfProduct;
        Weight weight = Weight.KG;
        double amount;
        double price;
        System.out.print(" Enter the name of new Product ");
        nameOfProduct = scanner.nextLine();
        nameOfProduct = nameOfProduct.trim();
        System.out.print(" Enter the color of Product ");
        colorOfProduct = scanner.nextLine();
        System.out.print(" Select a measurement type: 1=KG;  2=PIECE; (By default is KG) ");
        String kgOrPiece = scanner.nextLine();
        if (kgOrPiece.equals("2")) weight = Weight.PIECE;
        System.out.print(" Enter the amount of Product ");
        amount = scanner.nextDouble();
        System.out.print(" Enter the price of Product ");
        price = scanner.nextDouble();
        scanner.nextLine();

        boolean addedProduct = userService.addProduct(new ProductDTO(nameOfProduct,
                colorOfProduct, weight, amount, price), user);
    }

    private static void fillBalance(Scanner scanner, UserService userService, User user) {
        double money;
        System.out.println();
        System.out.print("How much do you want to deposit into your account? ");
        money = scanner.nextDouble();
        scanner.nextLine();
        userService.fillBalance(money, user);
    }

    private static void showProduct(UserService userService) {
        List<Product> availableproducts = userService.productList();
        if (availableproducts != null) {
            for (Product availableproduct : availableproducts) {
                System.out.println(availableproduct);
            }
        }
    }

    private static void buyProduct(Scanner scanner, UserService userService, User user) {
        String nameOfProduct, colorOfProduct;
        double mount;
        List<Product> availableproducts = userService.productList();
        if (availableproducts != null) {
            for (Product availableproduct : availableproducts) {
                System.out.println(availableproduct);
            }
            System.out.print("Choose product: ");
            nameOfProduct = scanner.nextLine();
            System.out.print("Choose color of product: ");
            colorOfProduct = scanner.nextLine();
            System.out.print("Enter the amount: ");
            mount = scanner.nextDouble();
            scanner.nextLine();
            ResponsDTO bought = userService.buyProduct(nameOfProduct, colorOfProduct, mount, user);
            System.out.println(bought.message());

        }
    }

    private static void register(Scanner scanner) {
//        List<User> userList = Database.getInstance().USERS;

        try {
            List<User> userList = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
        System.out.print("Enter user name: ");
        String userName = scanner.nextLine();
        if ((userList != null ? userList.size() : 0) != 0) {
            for (User user : userList) {
                if (user.getUsername().equals(userName)) {
                    System.out.println("This username already exists! ");
                    return;
                }
            }
        }
        System.out.print("Enter password: ");
        String passwordOfUser = scanner.nextLine();
        System.out.print("Enter name: ");
        String nameOfUser = scanner.nextLine();
        System.out.print("Enter lastname: ");
        String lastNameOfUser = scanner.nextLine();
        long balanceOfUser = 0;
        System.out.print("Enter email: ");
        String emailOfUser = scanner.nextLine();
        userList.add(new User(nameOfUser, passwordOfUser, nameOfUser, lastNameOfUser, balanceOfUser, emailOfUser));
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile,userList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
