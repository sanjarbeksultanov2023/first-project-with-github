package uz.pdp.onlineShop.server.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import uz.pdp.onlineShop.server.database.Database;
import uz.pdp.onlineShop.server.dtos.*;
import uz.pdp.onlineShop.server.enums.UserStatus;
import uz.pdp.onlineShop.server.enums.Weight;
import uz.pdp.onlineShop.server.model.Product;
import uz.pdp.onlineShop.server.model.User;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    static ObjectMapper objectMapper = new ObjectMapper();
    static File productsFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\products.json");
    static File usersFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\users.json");
    static File changedHistorysFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\changesHistory.json");
    static File targetHistorysFile = new File("C:\\g31_SultanovSanjar_jm4\\jm_4_lesson7\\first-project-with-github\\database\\history.txt\\targetHistory.json");

    public UserService() {
    }

    List<TradeHistory> tradeHistories = Database.getTradeHistories();
    List<User> users = Database.getInstance().USERS;
    List<Product> products = Database.productList;
    List<HistoryOfChangesDTO> historyOfChanges = Database.getHistoryOfChanges();


    //    private UserService instance=new UserService();
    public UserService getInstance() {
        return new UserService();
    }

    public static ResponsDTO makeAdmin(String lastUserName) {
        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user : userList1) {
                if (user.getUsername().equals(lastUserName)) {
                    user.setUserStatus(UserStatus.ADMIN);
                    objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile, userList1);
                    return new ResponsDTO("Done ", true);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponsDTO("Doesn't ", false);

//        List<User> userList = Database.getInstance().getUSERS();
//        for (User user1 : userList) {
//            if (user1.getUsername().equals(lastUserName)) {
//                user1.setUserStatus(UserStatus.ADMIN);
//                return new ResponsDTO("Done ", true);
//            }
//        }
//        return new ResponsDTO("Doesn't ", false);
    }


    public int countOfUsers() {
        List<User> userList1 = null;
        try {
            userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userList1 != null ? userList1.size() : 0;
    }

    public List<User> listOfUsers() {
        List<User> users1 = new ArrayList<>();
        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user : userList1) {
                if (user.getUserStatus().equals(UserStatus.USER)) {
                    users1.add(user);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        for (User user : users) {
//            if (user.getUserStatus().equals(UserStatus.USER)) {
//                users1.add(user);
//            }
//        }

        return users1;
    }

    public List<User> listOfAdmins() {
        List<User> users1 = new ArrayList<>();

        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user : userList1) {
                if (user.getUserStatus().equals(UserStatus.ADMIN)) {
                    users1.add(user);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        for (User user : users) {
//            if (user.getUserStatus().equals(UserStatus.ADMIN)) {
//                users1.add(user);
//            }
//        }
        return users1;
    }

    public void addUser(User user) {
        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            userList1.add(user);
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile, userList1);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        users.add(user);
    }

    public boolean removeUser(User user) {
        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            userList1.removeIf(user1 -> (user1.getUsername().equals(user.getUsername())));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile, userList1);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return users.removeIf(user1 -> (user1.getUsername().equals(user.getUsername())));
        return false;
    }

    public ResponsDTO checkUser(CheckUser user) {

        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user1 : userList1) {
                if (user1.getUsername().equals(user.username()) && user1.getPassword().equals(user.password())) {
                    return new ResponsDTO("User is found", true);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        for (User user1 : users) {
//            if (user1.getUsername().equals(user.username()) && user1.getPassword().equals(user.password())) {
//                return new ResponsDTO("User is found", true);
//            }
//        }
        return new ResponsDTO("User not found", false);
    }

    public List<Product> productList() {
        List<Product> availableproducts = new ArrayList<>();

        try {
            List<Product> productList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
            });
            for (Product product : productList) {
                if (product.getAmount() > 0) {
                    availableproducts.add(product);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        List<Product> products = Database.productList;
//        for (Product product : products) {
//            if (product.getAmount() > 0) {
//                availableproducts.add(product);
//            }
//        }
        return availableproducts;
    }

    public List<Product> productListFull() {
        try {
            return objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
//        return Database.productList;
    }

    public User findUser(String username) {

        try {
            List<User> userList1 = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user : userList1) {
                if (user.getUsername().equals(username)) return user;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        for (User user : users) {
//            if (user.getUsername().equals(username)) return user;
//        }
        return null;
    }

    public ResponsDTO buyProduct(String productName, String colorOfProduct, double mount, User user) {

        try {
            List<Product>  availableproducts = objectMapper.readValue(productsFile,new TypeReference<List<Product>>() {
            });
        for (Product product : availableproducts) {
            if (product.getNameOfProduct().equals(productName) && product.getColorOfProduct().equals(colorOfProduct)) {
                if (product.getAmount() > mount) {
                    if (mount * product.getPrice() <= user.getBalance()) {
                        if (product.getWeight().equals(Weight.KG)) {
                            user.setBalance((long) (user.getBalance() - mount * product.getPrice()));
                            product.setAmount(product.getAmount() - mount);
                            try {
                                List<User> userList = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
                                });
                                for (User user1 : userList) {
                                    if(user1.getUsername().equals(user.getUsername())){
                                        user1.setBalance(user.getBalance());
                                    }
                                }
                                objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile,userList);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile,availableproducts);
                        } else if (product.getWeight().equals(Weight.PIECE)) {
                            int amount = (int) Math.floor(mount);

                            product.setAmount(product.getAmount() - amount);
                            user.setBalance((long) (user.getBalance() - amount * product.getPrice()));
                            try {
                                List<User> userList = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
                                });
                                for (User user1 : userList) {
                                    if(user1.getUsername().equals(user.getUsername())){
                                        user1.setBalance(user.getBalance());
                                    }
                                }
                                objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile,userList);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile,availableproducts);
                        }
                        try {
                            List<TradeHistory> tradeHistories1 = objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
                            });
                            tradeHistories1.add(new TradeHistory(LocalDateTime.now().toString(), true, ("User name " + user.getUsername() +
                                    ";  Purchase was successful! Product: " + product.getColorOfProduct() + " "
                                    + productName + " " + mount + " " + product.getWeight() + " * " + product.getPrice()
                                    + "  SUM --> " + (mount * product.getPrice()))));
                            objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, tradeHistories1);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
//                        tradeHistories.add(new TradeHistory(LocalDateTime.now(), true, ("User name " + user.getUsername() +
//                                ";  Purchase was successful! Product: " + product.getColorOfProduct() + " "
//                                + productName + " " + mount + " " + product.getWeight() + " * " + product.getPrice()
//                                + "  SUM --> " + (mount * product.getPrice()))));


                        return new ResponsDTO("  Your purchase was successful! Thank you for your purchase!", true);
                    } else {
                        try {
                            List<TradeHistory> tradeHistories1 = objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
                            });
                            tradeHistories1.add(new TradeHistory(LocalDateTime.now().toString(), false, (user.getUsername() +
                                    "  There are insufficient funds in your account!")));
                            objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, tradeHistories1);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
//                        tradeHistories.add(new TradeHistory(LocalDateTime.now(), false, (user.getUsername() +
//                                "  There are insufficient funds in your account!")));
                        return new ResponsDTO("  There are insufficient funds in your account!", false);
                    }
                } else {
                    try {
                        List<TradeHistory> tradeHistories1 = objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
                        });
                        tradeHistories1.add(new TradeHistory(LocalDateTime.now().toString(), false, (user.getUsername() +
                                "  Product quantity exceeded the standard! Please check and re-enter!")));
                        objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, tradeHistories1);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
//                    tradeHistories.add(new TradeHistory(LocalDateTime.now(), false, (user.getUsername() +
//                            "  Product quantity exceeded the standard! Please check and re-enter!")));
                    return new ResponsDTO("  Product quantity exceeded the standard! Please check and re-enter!", false);
                }
            }
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            List<TradeHistory> tradeHistoryList = objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
            });
            tradeHistoryList.add(new TradeHistory(LocalDateTime.now().toString(), false, (user.getUsername() +
                    " entered product name incorrectly")));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, tradeHistoryList);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        tradeHistories.add(new TradeHistory(LocalDateTime.now(), false, (user.getUsername() +
//                " entered product name incorrectly")));
        return new ResponsDTO("  The product name was entered incorrectly", false);
    }

    public boolean fillBalance(double money, User user) {
        user.setBalance(user.getBalance() + (long) money);
        try {
            List<User> userList = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user1 : userList) {
                if (user1.getUsername().equals(user.getUsername())) {
                    user1.setBalance(user.getBalance());
                }
            }
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile,userList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            List<TradeHistory> tradeHistories1 = objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
            });
            tradeHistories1.add(new TradeHistory(LocalDateTime.now().toString(),
                    true, (user.getUsername() + " Fill balance to " + money)));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, tradeHistories1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        tradeHistories.add(new TradeHistory(LocalDateTime.now(), true, (user.getUsername() + " Fill balance to " + money)));
        return true;
    }

    public boolean addProduct(ProductDTO productDTO, User user) {
        if (user.getUserStatus().equals(UserStatus.ADMIN)) {
            try {
                List<Product> productList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
                });
                for (Product product : productList) {
                    if (product.getNameOfProduct().equals(productDTO.nameOfProduct())
                            && product.getColorOfProduct().equals(productDTO.colorOfProduct())) {

                        try {
                            List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
                            });
                            changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                                    (user.getUsername() + "  Tried to add an existing product"), true));
                            objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

//                    historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                            (user.getUsername() + "  Tried to add an existing product"), true));
                        return false;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                List<Product> productsList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
                });
                productsList.add(new Product(productDTO.nameOfProduct(),
                        productDTO.colorOfProduct(), productDTO.weight(), productDTO.amount(), productDTO.price()));
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile, productsList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            products.add(new Product(productDTO.nameOfProduct(),
//                    productDTO.colorOfProduct(), productDTO.weight(), productDTO.amount(), productDTO.price()));

            try {
                List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
                });
                changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                        (user.getUsername() + "  Added a new product!"), true));
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                    (user.getUsername() + "  Added a new product!"), true));
            return false;
        }
        return true;
    }


    public Product findProduct(String nameOfProduct, String colorOfProduct) {
        List<Product> productList = productListFull();
        for (Product product : productList) {
            if (product.getNameOfProduct().equals(nameOfProduct) && product.getColorOfProduct().equals(colorOfProduct))
                return product;
        }
        return null;
    }

    public boolean addAmountToProduct(Product product, double addAmount, User user) {
        if (user.getUserStatus().equals(UserStatus.ADMIN)) {
            try {
                List<Product> productList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
                });
                for (Product product1 : productList) {
                    if (product1.getNameOfProduct().equals(product.getNameOfProduct()) && product1.getColorOfProduct().equals(product.getColorOfProduct())) {
                        product1.setAmount(product.getAmount() + addAmount);
                    }
                }
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile, productList);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
                });
                changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                        (user.getUsername() + " added amount to product!"), true));
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                    (user.getUsername() + " added amount to product!"), true));
            return true;
        }

        try {
            List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
            });
            changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                    (user.getUsername() + " can not add amount to product!"), false));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                (user.getUsername() + " can not add amount to product!"), false));
        return false;
    }

    public boolean changePriceOfProduct(Product product, double newPrice, User user) {
        if (user.getUserStatus().equals(UserStatus.ADMIN)) {

            try {
                List<Product> productList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
                });
                for (Product product1 : productList) {
                    if (product1.getNameOfProduct().equals(product.getNameOfProduct()) && product1.getColorOfProduct().equals(product.getColorOfProduct())) {
                        product1.setPrice(newPrice);
                    }
                }
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile, productList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
                });
                changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                        (user.getUsername() + " changed price of product!"), true));
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                    (user.getUsername() + " changed price of product!"), true));
            return true;
        }

        try {
            List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
            });
            changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                    (user.getUsername() + " can not change price of product!"), false));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                (user.getUsername() + " can not change price of product!"), false));
        return false;
    }

    public boolean changeWeightOfProduct(Product product, Weight weight, User user) {
        if (user.getUserStatus().equals(UserStatus.ADMIN)) {
            try {
                List<Product> productList = objectMapper.readValue(productsFile, new TypeReference<List<Product>>() {
                });
                for (Product product1 : productList) {
                    if (product1.getNameOfProduct().equals(product.getNameOfProduct()) && product1.getColorOfProduct().equals(product.getColorOfProduct())) {
                        product.setWeight(weight);
                    }
                }
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(productsFile, productList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(changedHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
                });
                changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                        (user.getUsername() + " changed weight of product!"), true));
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(changedHistorysFile, changesHistoryList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                    (user.getUsername() + " changed weight of product!"), true));
            return true;
        }

        try {
            List<HistoryOfChangesDTO> changesHistoryList = objectMapper.readValue(targetHistorysFile, new TypeReference<List<HistoryOfChangesDTO>>() {
            });
            changesHistoryList.add(new HistoryOfChangesDTO(LocalDateTime.now().toString(), user.getUsername(),
                    (user.getUsername() + " can not change weight of product!"), false));
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(targetHistorysFile, changesHistoryList);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        historyOfChanges.add(new HistoryOfChangesDTO(LocalDateTime.now(), user.getUsername(),
//                (user.getUsername() + " can not change weight of product!"), false));
        return false;
    }

    public List<TradeHistory> getTradeHistory(User user) {
        if (!user.getUserStatus().equals(UserStatus.USER)) {
            try {
                return objectMapper.readValue(targetHistorysFile, new TypeReference<List<TradeHistory>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
//            return Database.getTradeHistories();
        }
        return null;
    }

    public static ResponsDTO makeUser(String lastAdminName) {
        try {
            List<User> userList = objectMapper.readValue(usersFile, new TypeReference<List<User>>() {
            });
            for (User user : userList) {
                if (user.getUsername().equals(lastAdminName)) {
                    user.setUserStatus(UserStatus.USER);
                    objectMapper.writerWithDefaultPrettyPrinter().writeValue(usersFile, userList);
                    return new ResponsDTO("Done ", true);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        List<User> userList = Database.getInstance().getUSERS();
//        for (User user : userList) {
//            if (user.getUsername().equals(lastAdminName)) {
//                user.setUserStatus(UserStatus.USER);
//                return new ResponsDTO("Done ", true);
//            }
//        }
        return new ResponsDTO("Doesn't ", false);
    }
}
