package uz.pdp.onlineShop.server.dtos;

import java.time.LocalDateTime;

public record HistoryOfChangesDTO(String localDateTime, String username, String message, boolean succes) {
}
