package uz.pdp.onlineShop.server.dtos;

import java.time.LocalDateTime;

public record TradeHistory(String localDateTime,boolean succes, String message) {
    @Override
    public String toString() {
        return  "Date and time=" + localDateTime +
                "; succes=" + succes +
                "; " + message + '\'';
    }
}
