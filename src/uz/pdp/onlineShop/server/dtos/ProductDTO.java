package uz.pdp.onlineShop.server.dtos;

import uz.pdp.onlineShop.server.enums.Weight;

public record ProductDTO(String nameOfProduct, String colorOfProduct, Weight weight, double amount, double price) {
}
