package uz.pdp.onlineShop.server.dtos;

public record CheckUser(String username, String password) {
}
