package uz.pdp.onlineShop.server.model;

import uz.pdp.onlineShop.server.enums.Weight;

public class Product {
    private String nameOfProduct;
    private String colorOfProduct;
    private Weight weight;
    private double amount;
    private double price;

    public Product() {
    }

    public Product(String nameOfProduct, String colorOfProduct, Weight weight, double amount, double price) {
        this.nameOfProduct = nameOfProduct;
        this.colorOfProduct = colorOfProduct;
        this.weight = weight;
        this.amount = amount;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public String getColorOfProduct() {
        return colorOfProduct;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public Weight getWeight() {
        return weight;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return
                "nameOfProduct='" + nameOfProduct + '\'' +
                ", colorOfProduct='" + colorOfProduct + '\'' +
                ", amount=" + amount +" "+weight+
                        ", price="+price+
                ";  ";
    }
}
