package uz.pdp.onlineShop.server.model;

import uz.pdp.onlineShop.server.enums.UserStatus;


public class User {
    private String username;
    private String password;
    private String name;
    private String lastName;
    private long balance;
    private String email;
    private UserStatus userStatus = UserStatus.USER;

    public User() {
    }

    public User(String username, String password, String name, String lastName, long balance, String email, UserStatus userStatus) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.balance = balance;
        this.email = email;
        this.userStatus = userStatus;
    }

    public User(String username, String password, String name, String lastName, long balance, String email) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.balance = balance;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", balance=" + balance +
                ", email='" + email + '\'' +
                ", userStatus=" + userStatus +
                '}';
    }
}
