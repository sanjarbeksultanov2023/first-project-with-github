package uz.pdp.onlineShop.server.database;

import uz.pdp.onlineShop.server.dtos.HistoryOfChangesDTO;
import uz.pdp.onlineShop.server.dtos.TradeHistory;
import uz.pdp.onlineShop.server.enums.UserStatus;
import uz.pdp.onlineShop.server.enums.Weight;
import uz.pdp.onlineShop.server.model.Product;
import uz.pdp.onlineShop.server.model.User;

import java.util.ArrayList;
import java.util.List;

public class Database {
    public Database() {
    }

    private static Database instance=new Database();

    public static Database getInstance(){
        return instance;
    }

    public final List<User> USERS =new ArrayList<>(){{
        User user = new User("Sanjar", "123", "Sanjar", "Sultanov", 120000, "ttt@gmail.com");
        user.setUserStatus(UserStatus.SUPERADMIN);
        add(user);
        User user1 = new User("Nodir", "123", "Nodir", "Sultanov", 120000, "tt@gmail.com");
        user1.setUserStatus(UserStatus.ADMIN);
        add(user1);
       add( new User("Husen","123","Husen","Teshayev",120000,"tstt@gmail.com"));
       add( new User("Ali","123","Asad","Valiyev",120000,"dttt@gmail.com"));
    }};

    public static List<Product>productList=new ArrayList<>(){{
        add(new Product("Apple","Green", Weight.KG,50,12000));
        add(new Product("Apple","Red", Weight.KG,0,20000));
        add(new Product("Watermelon","Green", Weight.PIECE,25,25000));
        add(new Product("Rice","White", Weight.KG,100,22000));
    }};
        static List<TradeHistory> tradeHistories;
    public  static List<TradeHistory> getTradeHistories(){
        if (tradeHistories==null) {
            tradeHistories=new ArrayList<>();
        }
        return tradeHistories;
    }
    static List<HistoryOfChangesDTO> historyOfChanges;
    public  static List<HistoryOfChangesDTO> getHistoryOfChanges(){
        if (historyOfChanges==null) {
            historyOfChanges = new ArrayList<>();
        }
        return historyOfChanges;
    }

    public List<User> getUSERS() {
        return USERS;
    }
}
